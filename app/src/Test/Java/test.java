import com.oreilly.demo.android.pa.uidemo.model.Dot;
import com.oreilly.demo.android.pa.uidemo.model.SpawnPoint;
import android.graphics.Color;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Created by jonahmurray on 12/15/16.
 */
public class test {
    @Test
    public void setDots (){
        SpawnPoint test_spawn = new SpawnPoint(0, 0);
        Dot testdot = new Dot(test_spawn, 0, 0, Color.BLACK, 45, true, 3, 15);
        test_spawn.setDot(testdot);
        assertEquals(true, test_spawn.hasDot);
    }

    @Test
    public void lose_life() {
        SpawnPoint test_spawn = new SpawnPoint(0, 0);
        Dot testdot = new Dot(test_spawn, 0, 0, Color.BLACK, 45, true, 3, 15);
        testdot.lose_life();
        assertEquals(2, testdot.getLives());
    }

    @Test
    public void deleteDot(){
        SpawnPoint test_spawn = new SpawnPoint(0, 0);
        Dot testdot = new Dot(test_spawn, 0, 0, Color.BLACK, 45, true, 3, 15);
        testdot.deleteDot(testdot);
        assertEquals(null, testdot);
    }

    @Test
    public void getX(){
        SpawnPoint test_spawn = new SpawnPoint(0, 0);
        Dot testdot = new Dot(test_spawn, 0, 0, Color.BLACK, 45, true, 3, 15);
        assertTrue(testdot.getX() == 0);
    }

    @Test
    public void getY(){
        SpawnPoint test_spawn = new SpawnPoint(0, 0);
        Dot testdot = new Dot(test_spawn, 0, 0, Color.BLACK, 45, true, 3, 15);
        assertTrue(testdot.getY() == 0);
    }

    @Test
    public void getColor(){
        SpawnPoint test_spawn = new SpawnPoint(0, 0);
        Dot testdot = new Dot(test_spawn, 0, 0, Color.BLACK, 45, true, 3, 15);
        assertTrue(testdot.getColor() == Color.BLACK);
    }

    @Test
    public void getDiameter(){
        SpawnPoint test_spawn = new SpawnPoint(0, 0);
        Dot testdot = new Dot(test_spawn, 0, 0, Color.BLACK, 45, true, 3, 15);
        assertTrue(testdot.getDiameter() == 45);
    }

    @Test
    public void isVulnerable(){
        SpawnPoint test_spawn = new SpawnPoint(0, 0);
        Dot testdot = new Dot(test_spawn, 0, 0, Color.BLACK, 45, true, 3, 15);
        assertTrue(testdot.is_vulnerable());
    }

    @Test
    public void change_vuln(){
        SpawnPoint test_spawn = new SpawnPoint(0, 0);
        Dot testdot = new Dot(test_spawn, 0, 0, Color.BLACK, 45, true, 3, 15);
        testdot.change_vuln();
        assertFalse(testdot.is_vulnerable());
    }
}

package com.oreilly.demo.android.pa.uidemo.view;

import android.content.Context;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.util.AttributeSet;
import android.view.View;

import com.oreilly.demo.android.pa.uidemo.model.Dot;
import com.oreilly.demo.android.pa.uidemo.model.Dots;
import com.oreilly.demo.android.pa.uidemo.model.SpawnPoint;



/**
 * I see spots!
 *
 * @author <a href="mailto:android@callmeike.net">Blake Meike</a>
 */
public class DotView extends View {

    private volatile Dots dots;

    /**
     * @param context the rest of the application
     */
    public DotView(final Context context) {
        super(context);
        setFocusableInTouchMode(true);
    }

    /**
     * @param context
     * @param attrs
     */
    public DotView(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        setFocusableInTouchMode(true);
    }

    /**
     * @param context
     * @param attrs
     * @param defStyle
     */
    public DotView(final Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);
        setFocusableInTouchMode(true);
    }

    /**
     * @param dots
     */
    public void setDots(final Dots dots) { this.dots = dots; }

    /**
     * @see android.view.View#onDraw(android.graphics.Canvas)
     */

    public int squares = 4;

    public static SpawnPoint[][] spawns = new SpawnPoint[4][4];


    /**
    @Override protected void setSpawns() {
        int x = 0;

        for (int i = getHeight()/(8); i < getHeight(); i += getHeight()/4)
        {
            for (int j = getWidth()/(8); j < getWidth(); j += getWidth()/4)
            {
                spawns [x] = new SpawnPoint(j,i, x);
                x++;

            }
        }
    }

     **/


    @Override protected void onDraw(final Canvas canvas) {
        final Paint paint = new Paint();
        paint.setStyle(Style.STROKE);
        paint.setColor(hasFocus() ? Color.BLUE : Color.GRAY);
        canvas.drawRect(0, 0, getWidth() - 1, getHeight() -1, paint);
/**
        int squares = 4;
        for (int i = 0; i < getHeight(); i+= getHeight()/squares)
        {
            for (int j = 0; j < getWidth(); j+= getWidth()/squares)
            {
                canvas.drawRect(i,j, getWidth()/squares, getHeight()/squares, paint);
            }
        }
**/
        if (null == dots) { return; }

        paint.setStyle(Style.FILL);
        for (final Dot dot : dots.getDots()) {
            paint.setColor(dot.getColor());
            canvas.drawCircle(
                dot.getX(),
                dot.getY(),
                dot.getDiameter(),
                paint);
        }




        int x = getWidth()/(2*squares);//initial spawn of pawn
        int y=getHeight()/(2*squares);

        for(int i=0;i<squares;i++){

            for(int j=0;j<squares;j++){
                spawns[i][j]=new SpawnPoint(x,y);
                x+=getWidth()/squares;


            }
            y+=getHeight()/squares;
            x = getWidth()/(2*squares);

        }

/**
        int x=0;
        for (int i = getHeight()/(8); i < getHeight(); i += getHeight()/4)
        {
            for (int j = getWidth()/(8); j < getWidth(); j += getWidth()/4)
            {
                spawns [x] = new SpawnPoint(j,i, x);
                x++;

            }
        }

**/
        for (int i = 0; i < squares; i++)
        {
            for(int j=0;j<squares;j++){
                canvas.drawCircle(spawns[i][j].getX(), spawns[i][j].getY(), 15, paint); //testing spawnpoints
            }

        }




    }
}

package com.oreilly.demo.android.pa.uidemo.model;
import java.util.Random;
import com.oreilly.demo.android.pa.uidemo.view.DotView;

import android.graphics.Color;
import android.view.View;
import java.util.Stack;
import java.util.Vector;
import java.util.*;





/** A dot: the coordinates, color and size. */
public final class Dot {
    private  float x, y;//starting x and y coordinate
    private  int color;//starting color
    private final int diameter;//diameter of dot
    private boolean vulnerable;//starting state
    private int lives;//starting lives
    private int tick_interval;
    private SpawnPoint position;//itnterval before dot change color and vulnerability and/moves
    ArrayList<int[]> moves=new ArrayList<int[]>();//array of possible translation when move function is called


    /**
     * @param x horizontal coordinate.
     * @param y vertical coordinate.
     * @param color the color.
     * @param diameter dot diameter.
     */
    public Dot(SpawnPoint start, final float x, final float y, final int color, final int diameter, boolean vulnerable, int lives, int tick_interval) {
        position = start;
        this.x = x;
        this.y = y;
        this.color = color;
        this.diameter = diameter;
        this.vulnerable = vulnerable;
        this.lives = lives;
        this.tick_interval = tick_interval;
        //following .add() methods populate the space arraylist
        int[] space={-1,-1};
        moves.add(space);
        space[0]=0;//0,-1
        moves.add(space);
        space[0]=1;//1,-1
        moves.add(space);
        space[0]=-1;
        space[1]=0;//-1,0
        moves.add(space);
        space[1]=1;//-1,1
        moves.add(space);
        space[0]=1;//1,1
        moves.add(space);
        space[0]=1;
        space[1]=0;
        moves.add(space);//1,0
        space[0]=0;
        space[1]=1;
        moves.add(space);//0,1
    }

    /** @return the horizontal coordinate. */
    public float getX() { return x; }

    /** @return the vertical coordinate. */
    public float getY() { return y; }

    /** @return the color. */
    public int getColor() { return color; }

    /** @return the dot diameter. */
    public int getDiameter() { return diameter; }

    /** @return the current state*/
    public boolean is_vulnerable() { return vulnerable; }

    /**Changes state */
    public void change_vuln(){
        vulnerable=!vulnerable;
    }

    /**changes color to signify state */
    public void change_color(int color){
        this.color=color;
    }

    /**@return the current lives */
    public int getLives() { return lives; }

    /**decrement total lives */
    public void lose_life(){
        if(getLives() != 0)
            this.lives -= 1;
    }

    /**randomly generate tick interval */
    public void set_tick_interval(){//set interval for movement and/or state change
        Random rn = new Random();
        this.tick_interval = rn.nextInt(24);
    }

    /**return the current position */
    public SpawnPoint getPosition() {
        return position;
    }

    /**removes itself */
    public void deleteDot(Dot dot) {
        dot = null;
    }







    public void Move(){
        Random rn = new Random();
        float xChange=0;
        float yChange=0;
        float xTemp=0;//holds new position until checked for validity
        float yTemp=0;
        while(moves.size()!=0){
            int index=rn.nextInt(moves.size());//pick the next position to check
            xChange=moves.get(index)[0];
            yChange=moves.get(index)[1];
            xTemp=x+xChange;
            yTemp=y+yChange;
            if(xTemp>0&&xTemp<5&&yTemp>0&&yTemp<5){
                for (Dot D:Dots.dots
                     ) {
                    if(D.x!=xTemp&&D.y!=yTemp){
                        //canvas.clearCircle(D.x,D.y,D.diameter);
                        D.x=xTemp;
                        D.y=yTemp;
                        //draw new dot
                    }
                }
            }
            moves.remove(index);//removes translation that is invalid
        }
    }





}
package com.oreilly.demo.android.pa.uidemo.model;

import android.graphics.Color;

/**
 * Created by jonahmurray on 12/13/16.
 */

public class SpawnPoint {
    private final int x,y;//coordinates of the spawn

    private Dot dot;//dot present at this location
    public boolean hasDot;//whether or not spawnpoint has a dot.  Initialized as false

    /**@return the x coordinate */
    public int getX() {
        return x;
    }

    /**@return the y oordinate */
    public int getY() {
        return y;
    }

    /**sets hasDot to true  */
    public boolean putDot () {
        return hasDot = true;
    }



    /**creates spawnpoint */
    public SpawnPoint (int x, int y){
        this.x = x;
        this.y = y;
        dot = null;
        hasDot = false;
    }

    /**sets spawnpoints dot to dot and changes hasdot */
    public void setDot(Dot dot) {

        this.dot = dot;
        putDot();

    }

}

package com.oreilly.demo.android.pa.uidemo;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.EditText;
import android.location.Location;

import com.oreilly.demo.android.pa.uidemo.model.Dot;
import com.oreilly.demo.android.pa.uidemo.model.Dots;
import com.oreilly.demo.android.pa.uidemo.model.SpawnPoint;
import com.oreilly.demo.android.pa.uidemo.view.DotView;


/** Android UI demo program */
public class TouchMe extends Activity {
    /** Dot diameter */
    public static final int DOT_DIAMETER = 45;

    /** Listen for taps. */
    private static final class TrackingTouchListener implements View.OnTouchListener {
        private final Dots mDots;
        private List<Integer> tracks = new ArrayList<>();

        TrackingTouchListener(final Dots dots) { mDots = dots; }

        //Spencer's territory
        @Override public boolean onTouch(final View v, final MotionEvent evt) {
            final int action = evt.getAction();

            if (action == MotionEvent.ACTION_DOWN) {
                int x = (int)evt.getX();
                int y = (int)evt.getY();
                Dot closest = new Dot(new SpawnPoint(0,0), 0, 0, Color.BLACK, DOT_DIAMETER, true, 5, 5);
                Integer old_close = Integer.MAX_VALUE;


                for (Dot d : mDots.getDots()) {
                    float[] results = new float[1];
                    Location.distanceBetween(x, y, d.getX(), d.getY(), results);
                    if (results[0] > old_close) {
                        closest = d;
                    }
                }

                //if it's within a radius of the dot's coordinates
                //if distance between is less than the radius, hit the dot.

                float results[] = new float[1];
                Location.distanceBetween(x, y, closest.getX(), closest.getY(), results);
                if(results[0] < DOT_DIAMETER) {
                    closest.lose_life();
                }
            }







            /*
            switch (action & MotionEvent.ACTION_MASK) {
                case MotionEvent.ACTION_DOWN:
                case MotionEvent.ACTION_POINTER_DOWN:
                    final int idx1 = (action & MotionEvent.ACTION_POINTER_INDEX_MASK)
                        >> MotionEvent.ACTION_POINTER_INDEX_SHIFT;
                    tracks.add(evt.getPointerId(idx1));
                    break;

                case MotionEvent.ACTION_POINTER_UP:
                    final int idx2 = (action & MotionEvent.ACTION_POINTER_INDEX_MASK)
                        >> MotionEvent.ACTION_POINTER_INDEX_SHIFT;
                    tracks.remove(evt.getPointerId(idx2));
                    break;




                default:
                    return false;
            }
            */



            return true;
        }

        private void addDot(
                final Dots dots,
                final SpawnPoint position,
                final float x,
                final float y,
                final float p,
                final float s) {
            dots.addDot(position, x, y, Color.CYAN, (int) ((p + 0.5) * (s + 0.5) * DOT_DIAMETER), false, 5, 1);
        }
    }

    //End of TrackingtouchListener class

    private final Random rand = new Random();

    /** The application model */
    private final Dots dotModel = new Dots();

    /** The application view */
    private DotView dotView;

    /** The dot generator */
    private Timer dotGenerator;

    /** Called when the activity is first created. */
    @Override public void onCreate(final Bundle state) {
        super.onCreate(state);

        // install the view
        setContentView(R.layout.main);

        // find the dots view
        dotView = (DotView) findViewById(R.id.dots);
        dotView.setDots(dotModel);

        dotView.setOnCreateContextMenuListener(this);
        dotView.setOnTouchListener(new TrackingTouchListener(dotModel));




        dotView.setOnKeyListener((final View v, final int keyCode, final KeyEvent event) -> {
            if (KeyEvent.ACTION_DOWN != event.getAction()) {
                return false;
            }

            int color;
            switch (keyCode) {
                case KeyEvent.KEYCODE_SPACE:
                    color = Color.MAGENTA;
                    break;
                case KeyEvent.KEYCODE_ENTER:
                    color = Color.BLUE;
                    break;
                default:
                    return false;
            }

            //Make a dot on this view in the color chosen above using the dotModel
            makeDot(dotModel, dotView, color);

            return true;
        });
        //End of setOnKeyListener

        // wire up the controller
        findViewById(R.id.button1).setOnClickListener((final View v) ->
            makeDot(dotModel, dotView, Color.RED)
        );
        findViewById(R.id.button2).setOnClickListener((final View v) ->
            makeDot(dotModel, dotView, Color.GREEN)
        );

        final EditText tb1 = (EditText) findViewById(R.id.text1);
        final EditText tb2 = (EditText) findViewById(R.id.text2);
        dotModel.setDotsChangeListener((final Dots dots) -> {
            final Dot d = dots.getLastDot();
            tb1.setText((null == d) ? "" : String.valueOf(d.getX()));
            tb2.setText((null == d) ? "" : String.valueOf(d.getY()));
            dotView.invalidate();
        });
        makeDot(dotModel, dotView, Color.BLACK);
    }
    //End of onCreate






    @Override public void onResume() {
        super.onResume();
        if (dotGenerator == null) {
            dotGenerator = new Timer();
            // generate new dots, one every two seconds
            dotGenerator.schedule(new TimerTask() {
                @Override
                public void run() {
                    // must invoke makeDot on the UI thread to avoid
                    // ConcurrentModificationException on list of dots

                    //makeDot(dotModel, dotView, Color.BLACK);
                }
            }, /*initial delay*/ 0, /*periodic delay*/ 2000);
        }
    }

    @Override public void onPause() {
        super.onPause();
        if (dotGenerator != null) {
            dotGenerator.cancel();
            dotGenerator = null;
        }
    }

    /** Install an options menu. */
    @Override public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.simple_menu, menu);
        return true;
    }

    /** Respond to an options menu selection. */
    @Override public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_clear:
                dotModel.clearDots();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /** Install a context menu. */
    @Override public void onCreateContextMenu(
            final ContextMenu menu,
            final View v,
            final ContextMenuInfo menuInfo) {
        menu.add(Menu.NONE, 1, Menu.NONE, "Clear").setAlphabeticShortcut('x');
    }

    /** Respond to a context menu selection. */
    @Override public boolean onContextItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                dotModel.clearDots();
                return true;
            default:
                return false;
        }
    }

    /**
     * @param dots the dots we're drawing
     * @param view the view in which we're drawing dots
     * @param color the color of the dot
     */

     /*
        for (int z = 0; z < 5; z++) {
            Random rand = new Random();
            int spawnLocation_int = rand.nextInt(15);
            if (dotView.spawns[spawnLocation_int].hasdot() != true) {
                dotView.spawns[spawnLocation_int].setDot(new Dot(dotView.spawns[spawnLocation_int].getX(),
                        dotView.spawns[spawnLocation_int].getY(), Color.MAGENTA, DOT_DIAMETER,
                        true, 5, 5));
            }
        }
        */

    void makeDot(final Dots dots, final DotView view, final int color) {
        //We're only making dots at the beginning of the game.
        //So first step, get dots to spawn only on spawn points.
        //Only add a dot if it's on a spawn point

        Random rand = new Random();
        int spawn_x = rand.nextInt(4);
        int spawn_y = rand.nextInt(4);
        SpawnPoint spawnPosition;
        spawnPosition = view.spawns[spawn_x][spawn_y];
        for (int i = 0; i < 3; i++)
        {
            if (spawnPosition.hasDot == false)
            {
                dots.addDot(spawnPosition, spawnPosition.getX(), spawnPosition.getY(),
                        color, DOT_DIAMETER, true, 5, 5);
                spawn_x = rand.nextInt(4);
                spawn_y = rand.nextInt(4);
                spawnPosition = view.spawns[spawn_x][spawn_y];
            }
            else {
                while(spawnPosition.hasDot == true)
                {
                    spawn_x = rand.nextInt(4);
                    spawn_y = rand.nextInt(4);
                    spawnPosition = view.spawns[spawn_x][spawn_y];

                }
                dots.addDot(spawnPosition, spawnPosition.getX(), spawnPosition.getY(),
                        color, DOT_DIAMETER, true, 5, 5);
            }

        }


        //view.spawns[spawn_int].setDot(Dot dot);





    }


}